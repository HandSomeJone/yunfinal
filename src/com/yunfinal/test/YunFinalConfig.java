package com.yunfinal.test;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.template.Engine;

/***
 * YunFinalConfig
 * 
 *  项目WebSocket需要使用tomcat7+ JDK７＋　的版本
 * 
 * @author dufuzhong
 *
 */
public class YunFinalConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
	}

	@Override
	public void configEngine(Engine me) {
		me.setDevMode(true);
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new WebSocketHandler());
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void configPlugin(Plugins me) {
		// TODO 自动生成的方法存根

	}

	@Override
	public void configRoute(Routes me) {
		me.add("/" , IndexController.class);
	}

}
