package com.yunfinal.test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

/***
 * /websocket/ 放行
 * @author dufuzhong
 *
 */
public class WebSocketHandler extends Handler {
	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		if (( target.indexOf('.') != -1 ) || target.startsWith("/websocket/")){
			return;
		}
		next.handle(target, request, response, isHandled);
	}
}
